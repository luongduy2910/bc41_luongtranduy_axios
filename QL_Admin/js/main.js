const BASE_URL = "https://63bea7fae348cb0762149a43.mockapi.io" ; 
function fetchDSGV() {
    batLoading() ; 
    axios ({
        url : `${BASE_URL}/QLND` , 
        method : "GET", 
    })
    .then(function(res){
        tatLoading() ; 
        renderDSGV(res.data) ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}
// TODO: Render QLND khi user load lại trang 
fetchDSGV() ; 

// TODO: Xây dựng chức năng xóa người dùng 

function xoaND(id) {
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLND/${id}` , 
        method : "DELETE" , 
    })
    .then(function(res){
        tatLoading() ; 
        fetchDSGV() ;
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}

// TODO: Xây dựng chức năng thêm người dùng mới 

function btnThemNguoiDung(){
    batLoading() ; 
    axios({
        url : "https://63bea7fae348cb0762149a43.mockapi.io/QLND" , 
        method : "GET" ,
    })
    .then(function(res){
        var isValid = kiemTraTrung(res.data) && kiemTraTaiKhoan() ; 
        isValid = isValid 
        & kiemTraHoTen() 
        & kiemTraEmail() 
        & kiemTraMatKhau() 
        & kiemTraHinhAnh() 
        & kiemTraLoaiND() 
        & kiemTraNgonNgu() 
        & kiemTraMoTa() ; 
        if (isValid) {
            axios({
                url : `${BASE_URL}/QLND` , 
                method : 'POST',
                data : layThongTinTuForm() , 
            })
            .then(function(res){
                    // * Render dữ liệu khi admin thêm người dùng thành công 
                    tatLoading() ; 
                    fetchDSGV() ; 
                    lamMoiForm() ; 
                    alert('Thêm người dùng thành công') ; 
            })
            .catch(function(err){
                tatLoading() ; 
                console.log(err);
            })
        }else {
            tatLoading() ; 
            alert('Thêm người dùng thất bại') ; 
        }
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}
function btnDongForm() {
    dongForm() ; 
}

// TODO: Xây dựng chức năng cập nhật thông tin người dùng 

function suaND(id){
    batLoading() ; 
    axios({
        url : `${BASE_URL}/QLND/${id}` , 
        method : "GET" , 
    })
    .then(function(res){
        tatLoading() ; 
        moForm() ; 
        showThongTinLenForm(res.data) ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
    document.getElementById('identify').value = id ; 
}

function capNhatND() {
    batLoading() ; 
        var isValid = kiemTraTaiKhoan() 
        & kiemTraHoTen() 
        & kiemTraEmail() 
        & kiemTraMatKhau() 
        & kiemTraHinhAnh() 
        & kiemTraLoaiND() 
        & kiemTraNgonNgu() 
        & kiemTraMoTa() ; 
        if(isValid) {
            var idEl = document.getElementById('identify').value ; 
            axios({
                url : `${BASE_URL}/QLND/${idEl}` , 
                method : "PUT" , 
                data : layThongTinTuForm() , 
            })
            .then(function(res) {
                tatLoading() ; 
                // * Render dữ liệu sau khi cập nhật thành công 
                fetchDSGV() ; 
                dongFormCapNhat() ; 
                document.getElementById('TaiKhoan').disabled = false ; 
                alert('Thêm người dùng thành công') ; 
                lamMoiForm() ; 
            })
            .catch(function(err) {
                tatLoading() ; 
                console.log(err);
            })
        }
        else {
            tatLoading() ; 
            alert('Cập nhật người dùng thất bại') ; 
        }
}

