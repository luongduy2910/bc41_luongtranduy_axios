function kiemTraTrung(arrND){
    var flag = true ; 
    var tk = document.getElementById('TaiKhoan').value ; 
    var viTri = arrND.findIndex(function(item){
        return item.taiKhoan == tk  ;
    })
    if(viTri != -1){
        document.getElementById('tk__error').innerHTML = "Tài khoản đã tồn tại" ;
        flag = false ;  
    }
    else{
        document.getElementById('tk__error').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraTaiKhoan() {
    var flag = true ; 
    var tk = document.getElementById('TaiKhoan').value ; 
    if (tk == "") {
        document.getElementById('tk__error').innerHTML = "Tài khoản không được bỏ trống" ;
        flag = false ;  
    }
    else{
        document.getElementById('tk__error').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraHoTen(){
    var flag = true ; 
    var hoTen = document.getElementById('HoTen').value ; 
    var letters = /^[a-zA-Z ]+$/;
    if(letters.test(hoTen)){
        document.getElementById('ht__error').innerHTML = "" ; 
        flag = true ; 
    }else if(hoTen == ""){
        document.getElementById('ht__error').innerHTML = "Họ tên không được để trống" ; 
        flag = false ; 
    }else {
        document.getElementById('ht__error').innerHTML = "Họ Tên không hợp lệ"  ;
        flag = false;
    }
    return flag ;
}

function kiemTraEmail(){
    var flag = true ; 
    var email = document.getElementById('Email').value ; 
    var letters = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})*$/
    if (letters.test(email)){
        document.getElementById('email__error').innerHTML = "" ; 
        flag = true ; 
    }else if(email == ""){
        document.getElementById('email__error').innerHTML = "Email không được để trống" ;
        flag = false ; 
    }else{
        document.getElementById('email__error').innerHTML = "Email không hợp lệ" ; 
        flag = false ;
    }
    return flag ;


}

function kiemTraMatKhau(){
    var flag = true ; 
    var mk = document.getElementById('MatKhau').value ; 
    var letters = /(?=(.*[0-9]))(?=.*[\!@#$%^&*()\\[\]{}\-_+=~`|:;"'<>,./?])(?=.*[a-z])(?=(.*[A-Z]))(?=(.*)).{6,8}/ ; 
    if (letters.test(mk)){
        document.getElementById('mk__error').innerHTML = "" ; 
        flag = true ; 
    }else if(mk == ""){
        document.getElementById('mk__error').innerHTML = "Mật khẩu không được để trống" ;
        flag = false ; 
    }else{
        document.getElementById('mk__error').innerHTML = "Mật Khẩu không hợp lệ" ; 
        flag = false ;
    }
    return flag ;

}

function kiemTraHinhAnh(){
    var flag = true ; 
    var ha = document.getElementById('HinhAnh').value ; 
    if(ha == ""){
        document.getElementById('ha__error').innerHTML = "Hình ảnh không được để trống" ;
        flag = false ; 
    }else{
        document.getElementById('ha__error').innerHTML = "" ; 
        flag = true ;
    }
    return flag ;
}

function kiemTraLoaiND() {
    var flag = true ; 
    var loai = document.getElementById('loaiNguoiDung').value ; 
    if (loai == "Chọn loại người dùng") {
        document.getElementById('loai__error').innerHTML = "Loại ND không được để trống" ; 
        flag = false ;  
    }
    else {
        document.getElementById('loai__error').innerHTML = "" ; 
        flag = true ;  
    }
    return flag ;
}

function kiemTraNgonNgu(){
    var flag = true ; 
    var nn = document.getElementById('loaiNgonNgu').value ; 
    if (nn == "Chọn ngôn ngữ") {
        document.getElementById('nn__error').innerHTML = "Ngôn ngữ không được để trống" ; 
        flag = false ;  
    }
    else {
        document.getElementById('nn__error').innerHTML = "" ; 
        flag = true ;  
    }
    return flag ;
}

function kiemTraMoTa(){
    var flag = true ; 
    var mt = document.getElementById('MoTa').value ; 
    if (mt == ""){
        document.getElementById('mt__error').innerHTML = "Mô tả không được để trống" ; 
        flag = false ; 
    }else if(mt.length > 60) {
        document.getElementById('mt__error').innerHTML = "Mô tả không được vượt quá 60 ký tự" ; 
        flag = false ; 
    }else {
        document.getElementById('mt__error').innerHTML = "" ; 
        flag = true ; 
    }
    return flag ;
}




