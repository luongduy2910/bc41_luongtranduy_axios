function renderDSGV(arr) {
    var contentHTML = "" ; 
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        var contentTr = 
        `
        <tr>
            <td>${element.id}</td>
            <td>${element.taiKhoan}</td>
            <td>${element.matKhau}</td>
            <td>${element.hoTen}</td>
            <td>${element.email}</td>
            <td>${element.ngonNgu}</td>
            <td>${element.loaiND}</td>
            <td><button onclick = "xoaND(${element.id})" class = "btn btn-primary">Xóa</button>
                <button data-toggle="modal" data-target="#myModal" onclick = "suaND(${element.id})" class = "btn btn-warning">Sửa</button>
            </td>           
        </tr>
        `
        contentHTML += contentTr ; 
        
    }
    document.getElementById('tblDanhSachNguoiDung').innerHTML = contentHTML ; 
}

function layThongTinTuForm(){
    var taiKhoan = document.getElementById('TaiKhoan').value ;
    var hoTen = document.getElementById('HoTen').value ;
    var matKhau = document.getElementById('MatKhau').value ;
    var email = document.getElementById('Email').value ;
    var loaiND = document.getElementById('loaiNguoiDung').value ;
    var ngonNgu = document.getElementById('loaiNgonNgu').value ;
    var moTa = document.getElementById('MoTa').value ;
    var hinhAnh = document.getElementById('HinhAnh').value ;
    var nd = new Nguoidung(
        taiKhoan , 
        hoTen , 
        matKhau ,
        email , 
        loaiND , 
        ngonNgu , 
        moTa , 
        hinhAnh) ;
    return nd ;    
}

function showThongTinLenForm(data){
    document.getElementById('TaiKhoan').disabled = true ; 
    document.getElementById('TaiKhoan').value = data.taiKhoan ; 
    document.getElementById('HoTen').value = data.hoTen ; 
    document.getElementById('MatKhau').value = data.matKhau ; 
    document.getElementById('Email').value = data.email ; 
    document.getElementById('loaiNguoiDung').value = data.loaiND ;
    document.getElementById('loaiNgonNgu').value = data.loaiND ; 
    document.getElementById('MoTa').value = data.moTa ; 
    document.getElementById('HinhAnh').value = data.hinhAnh ; 

}

function dongFormCapNhat() {
    var btncapNhat = document.getElementById('capNhatND') ; 
    btncapNhat.style.display = 'none' ;
    var btnthem = document.getElementById('themND') ; 
    btnthem.style.display = 'block' ;
}

function moForm(){ 
    var btnthem = document.getElementById('themND') ; 
    btnthem.style.display = 'none' ;
    var btncapNhat = document.getElementById('capNhatND') ; 
    btncapNhat.style.display = 'block' ;
}

function lamMoiForm() {
    document.getElementById('TaiKhoan').value = "" ;
    document.getElementById('HoTen').value = "" ;
    document.getElementById('MatKhau').value = "" ;
    document.getElementById('Email').value = "" ;
    document.getElementById('loaiNguoiDung').value = "" ;
    document.getElementById('loaiNgonNgu').value = "" ;
    document.getElementById('MoTa').value = "" ;
    document.getElementById('HinhAnh').value = "" ;
}

