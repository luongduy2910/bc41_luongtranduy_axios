function renderDSGV(arr){
    var contentHTML = "" ; 
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if(element.loaiND == "GV") {
            var contentDiv =
            `
                <div class="col-3 card text-center">
                    <img class="card-img-top" src="./img/${element.hinhAnh}" alt="">
                    <div class="card-body">
                        <span class="language" >${element.ngonNgu}</span>
                        <h5 class="card-title">${element.hoTen}</h5>
                        <p class="card-text">${element.moTa}</p>
                    </div>
                </div>
            `
            contentHTML += contentDiv ; 
        }
        else{
            continue ; 
        }
        
    }
    document.getElementById('list').innerHTML = contentHTML ; 
}

function fetchDSGV(){
    batLoading() ; 
    var BASE_URL = `https://63bea7fae348cb0762149a43.mockapi.io` ; 
    axios({
        url : `${BASE_URL}/QLND` , 
        method : "GET" , 
    })
    .then(function(res){
        tatLoading() ; 
        renderDSGV(res.data) ; 
    })
    .catch(function(err){
        tatLoading() ; 
        console.log(err);
    })
}

fetchDSGV() ; 